(function() {

    var GroceryConfig = function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("home", {
                url: "/index",
                templateUrl: "./index.html",
                controller: "GroceryCtrl as groceryCtrl"
            })
            .state("edit", {
                url: "/edit",
                templateUrl: "/views/edit.html",
                controller: "EditCtrl as editCtrl"
            });
        $urlRouterProvider.otherwise("/home");
    }
    GroceryConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ];
    
    var EditCtrl = function($state) {
        console.info("initializing editCtrl");
        var editCtrl = this;
        editCtrl.back = function() {
            $state.go("home");
        }
    }
    EditCtrl.$inject = [ "$state" ];
    
}) ();

