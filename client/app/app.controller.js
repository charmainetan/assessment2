(function() {
    var GroceryApp = angular.module("GroceryApp", []);
    

    var GroceryConfig = function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("home", {
                url: "/index",
                templateUrl: "./index.html",
                controller: "GroceryCtrl as groceryCtrl"
            })
            .state("edit", {
                url: "/edit",
                params: {
                    upcid: 0,
                    brand: "",
                    productname: ""
                },
                templateUrl: "/views/editproduct.html",
                controller: "EditCtrl as editCtrl"
            });
        $urlRouterProvider.otherwise("/home");
    }
    GroceryConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ];



    var GrocerySvc = function ($http, $q) {
        var grocerySvc = this;

        grocerySvc.showAllProducts = function() {
            var defer = $q.defer();
            $http.get("/grocery")
                .then(function(result) {
                    defer.resolve(result.data);
                }).catch(function(err) {
                    defer.reject(err);
                });
            return(defer.promise);
        };

        grocerySvc.sortBrandDesc = function() {
            var defer = $q.defer();
            $http.get("/grocery/branddesc")
                .then(function(result) {
                    defer.resolve(result.data);
                }).catch(function(err) {
                    defer.reject(err);
                });
            return(defer.promise);
        };

        grocerySvc.sortBrandAsc = function() {
            var defer = $q.defer();
            $http.get("/grocery/brandasc")
                .then(function(result) {
                    defer.resolve(result.data);
                }).catch(function(err) {
                    defer.reject(err);
                });
            return(defer.promise);
        };


        grocerySvc.searchProducts = function(searchTerm) {
            var defer = $q.defer();
            $http.get("/getProducts/" + searchTerm)
                .then(function(result) {
                    defer.resolve(result.data);
                }).catch(function(err) {
                    defer.reject(err);
                });
                return(defer.promise);
        };

        grocerySvc.findProductById = function(productId) {
            var defer = $q.defer();
            $http.get("/getProductDetails/" + productId)
                .then(function(result){
                    defer.resolve(result.data);
                }).catch(function(err) {
                    defer.reject(err)
                });
                return(defer.promise);
        };

        // grocerySvc.searchByBrand = function(searchTerm) {
        //     var defer = $q.defer();
        //     $http.get("/api/grocery/" + searchTerm)
        //         .then(function(result) {
        //             console.log(result);
        //             defer.resolve(result.data);
        //         }).catch(function(err) {
        //             defer.reject(err);
        //         });
        //         return(defer.promise);
        // };

        // grocerySvc.searchByProduct = function(productName) {
        //     var defer = $q.defer();
        //     $http.get("/api/grocery/" + productName)
        //         .then(function(result) {
        //             console.log(result);
        //             defer.resolve(result.data);
        //         }).catch(function(err) {
        //             defer.reject(err);
        //         });
        //         return(defer.promise);
        // };


    };
    GrocerySvc.$inject = ["$http", "$q"];

    var GroceryCtrl = function(GrocerySvc) {
        var groceryCtrl = this;
        groceryCtrl.searchTerm = "";

        GrocerySvc.showAllProducts()
            .then(function(result) {
                console.info(">>> groceries shown!", result);
                groceryCtrl.groceries = result;
            }).catch(function(err) {
                console.error("error = ", err);
            })

        groceryCtrl.sortBrandDesc = function() {
            GrocerySvc.sortBrandDesc()
            .then(function(result) {
                console.info(result);
                groceryCtrl.groceries = result;
            }).catch(function(err) {
                console.log("sort desc err = ",err);
            })
        };

        groceryCtrl.sortBrandAsc = function () {
            GrocerySvc.sortBrandAsc()
            .then(function(result){
                console.info(result);
                groceryCtrl.groceries = result;
            }).catch(function(err) {
                console.log("sort asc err = ",err);
            })
        };

        groceryCtrl.editProduct = function(id,brand,productname) {
            console.log(">>>>Clicked edit , %d", id, brand, productname);
            $state.go("edit", { upcid: id, brand: brand, productname: productname  });
        }

        groceryCtrl.searchProducts = function() {
            GrocerySvc.searchProducts(groceryCtrl.searchTerm)
                .then(function(result) {
                    groceryCtrl.groceries = result;
                    groceryCtrl.notfound = false;
                }).catch(function(err) {
                    groceryCtrl.notfound = true;
                })
        };

    };


        // groceryCtrl.getSearchResults = function() {
        //     console.log(">>>>Search for %s", groceryCtrl.searchTerm);
        //     GrocerySvc.searchByBrand(groceryCtrl.searchTerm)
        //         .then(function(result) {
        //             console.info(">> search results... ", result);
        //             groceryCtrl.groceries = result;
        //             groceryCtrl.notfound = false;      
        //         }).catch(function(err) {
        //             console.error("search error = ", err);
        //             groceryCtrl.notfound = true;
                    
        //         })
        //     }
        // groceryCtrl.getProducts = function() {
        //     GrocerySvc.searchByProduct(groceryCtrl.productName)
        //         .then(function(result){
        //             groceryCtrl.groceries = result;
        //             groceryCtrl.notfound = false;
        //         }).catch(function(err) {
        //             console.error("search error = ", err);
        //             groceryCtrl.notfound = true;
        //         })
        // }


    // var EditCtrl = function($state) {
    //     var editCtrl = this;
    //     editCtrl.productId = "";

    //     editCtrl.productInfo = function() {
    //         GrocerySvc.findProductById(editCtrl.productId)
    //             .then(function(result){
    //                 editCtrl.product = result;
    //                 $state.go("edit", {product: p})
    //             }).catch(function(err) {
    //                 console.error("err in finding product ", err);
    //             })
    //     };
    // };
    // EditCtrl.$inject = ["$state"];



    GroceryCtrl.$inject = ["GrocerySvc"];

    GroceryApp.service("GrocerySvc", GrocerySvc);
    GroceryApp.controller("GroceryCtrl", GroceryCtrl);
    // GroceryApp.controller("EditCtrl", EditCtrl);


}) ();