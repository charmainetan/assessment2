const q = require("q");
const path = require("path");
const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");


const grocerydb = mysql.createPool({
    host:"localhost", port: 3306,
    user: "been", password: "been",
    database: "grocery_list",
    connectionLimit: 4
});

const mkQuery = function(sql, pool) {
	return (function() {
		const defer = q.defer();
		var params = [];
		for (var i in arguments)
			params.push(arguments[i]);

		pool.getConnection(function(err, conn) {
			if (err) {
				defer.reject(err);
				return;
			}
			conn.query(sql, params, function(err, result) {
				if (err)
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			});
		});
		return (defer.promise);
	});
};


const SHOW_ALL_PRODUCTS = "SELECT * from GROCERY_LIST ORDER BY ID LIMIT 20";
const SORT_BRAND_AZ = "SELECT * from GROCERY_LIST ORDER BY Brand LIMIT 20";
const SORT_BRAND_ZA = "SELECT * from GROCERY_LIST ORDER BY Brand DESC LIMIT 20";

// const SEARCH_BY_BRAND = "SELECT * from GROCERY_LIST where BRAND like ? LIMIT 20";
// const SEARCH_BY_PRODUCT = "SELECT * from GROCERY_LIST where NAME like ? LIMIT 20";

const SEARCH_PRODUCTS = "SELECT * from GROCERY_LIST where NAME like ? or BRAND like ? LIMIT 20";

const FIND_PRODUCT_BY_ID = "SELECT * from GROCERY_LIST where id=?"


const showAllProducts = mkQuery(SHOW_ALL_PRODUCTS, grocerydb);
const sortBrandAsc = mkQuery(SORT_BRAND_AZ, grocerydb);
const sortBrandDesc = mkQuery(SORT_BRAND_ZA, grocerydb);

// const searchByBrand = mkQuery(SEARCH_BY_BRAND, grocerydb);
// const searchByProduct = mkQuery(SEARCH_BY_PRODUCT, grocerydb);

const findProductById = mkQuery(FIND_PRODUCT_BY_ID, grocerydb);

const app = express();

app.get("/grocery", function(req,resp) {
	showAllProducts()
		.then(function(result) {
			resp.status(200);
			resp.type("application/json");
			resp.json(result);
		}).catch(function(err) {
			resp.status(500);
			resp.type("application/json");
			resp.json(err);
		});
});

app.get("/grocery/brandasc", function(req,resp) {
	sortBrandAsc()
		.then(function(result) {
			resp.status(200);
			resp.type("application/json");
			resp.json(result);
		}).catch(function(err) {
			resp.status(500);
			resp.type("application/json");
			resp.json(err);
		});
});

app.get("/grocery/branddesc", function(req,resp) {
	sortBrandDesc()
		.then(function(result) {
			resp.status(200);
			resp.type("application/json");
			resp.json(result);
		}).catch(function(err) {
			resp.status(500);
			resp.type("application/json");
			resp.json(err);
		});
});

// app.get("/api/grocery/:term", function(req,resp) {
// 	const searchTerm = "%" + req.params.term + "%"
// 	console.log("search term = %s", searchTerm);
// 	searchByBrand(searchTerm)
// 		.then(function(result) {
// 			if (result.length>0) {
// 				console.log(result)
// 				resp.status(200);
// 				resp.type("application/json");
// 				resp.json(result);
// 			} else {
// 				resp.status(404);
// 				resp.type("application/json");
// 				resp.end();
// 			}
// 		});
// });

// app.get("/api/grocery/:product", function(req,resp) {
// 	const productName = "%" + req.params.product + "%"
// 	console.log("product is = %s", productName);
// 	searchByProduct(productName)
// 		.then(function(result) {
// 			if (result.length>0) {
// 				console.log(result)
// 				resp.status(200);
// 				resp.type("application/json");
// 				resp.json(result);
// 			} else {
// 				resp.status(404);
// 				resp.type("application/json");
// 				resp.end();
// 			}
// 		});
// });


app.get("/getProducts/:term", function(req,resp){
	const searchTerm = "%" + req.params.term + "%";
	var searchResult = "";

    grocerydb.getConnection(function(err, conn) {
        if (err) {
            handleError(err, resp);
            return;
		}
		conn.query(SEARCH_PRODUCTS, [searchTerm, searchTerm], 
				function(err, result){
            if (err) {
                resp.status(500);
                resp.end(JSON.stringify(err));
                conn.release();
                return;
			}
			searchResult = result;
			resp.status(200);
			resp.type("application/json");
			resp.json(searchResult);
		})
		conn.release();
		return;
	})
});

app.get("/getProductDetails/:id", function(req, resp) {
	const productId = req.params.id;
	console.log(req.params.id);
	
	findProductById(productId)
		.then(function(result) {
			resp.status(200);
			resp.type("application/json");
			console.log(result);
			resp.json(result);
			resp.end();
		}).catch(function(err) {
			resp.status(500);
			resp.type("application/json");
			resp.json(err);
		});
	});

app.use(express.static(path.join(__dirname, "../client")));

app.use("/libs", 
	express.static(path.join(__dirname, "../bower_components")));

const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.info("Application started at %s on port %d", new Date(), port);
});
